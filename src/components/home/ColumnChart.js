import React from 'react';
import ReactApexChart from 'react-apexcharts';


export default class ColumnChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {

      series: [{
        data: [1000, 2000, 3000, 4000, 5000]
      }],
      options: {
        chart: {
          height: 200,
          type: 'bar',
          events: {
            click: function (chart, w, e) {
              // console.log(chart, w, e)
            }
          }
        },
        colors: ['#2E93fA', '#66DA26', '#546E7A', '#E91E63', '#FF9800'],
        plotOptions: {
          bar: {
            columnWidth: '30%',
            distributed: true
          }
        },
        dataLabels: {
          enabled: false
        },
        legend: {
          show: false
        },
        xaxis: {
          categories: [
            'Amber',
            'Feb',
            'Mar',
            'Apr',
            'May'
          ],
          labels: {
            style: {
              fontSize: '12px'
            }
          }
        }
      },
    };
  }

  render() {
    return (
      <div className="mixed-chart">
        <ReactApexChart className="w-100"
          options={this.state.options}
          series={this.state.series}
          type="bar"
          width="500"
        />
      </div>
    );
  }
}

