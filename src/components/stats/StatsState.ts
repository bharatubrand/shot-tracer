export interface StatsState{
    statsTable:Array<TableData>;
}

interface TableData{
    kid:number;
    name:string;
    subName:string;
    clubSpeed:number;
    attackAngle:number;
    ballSpeed:number;
    smashFactory:number;
    launchAng:number;
    spinRate:number;
    maxHeight:number;
    landAngle:number;
    carry:number;

}