import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import '../src/style/flaticon.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorker from './serviceWorker';
import RouterComponent from './components/router/RouterComponent';
import SignInComponent from './components/signin/SignInComponent';


ReactDOM.render(
  <React.StrictMode>
    <SignInComponent />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
