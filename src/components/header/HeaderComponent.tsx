import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import MenuIcon from '@material-ui/icons/Menu';
import MenuOpenIcon from '@material-ui/icons/MenuOpen';
import SettingsIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import { Toolbar, IconButton, Button } from '@material-ui/core';
import { HeaderProps } from './HeaderProps';
import { HeaderState } from './HeaderState';
export default class HeaderComponent extends React.Component<HeaderProps, HeaderState>{
    constructor(props:HeaderProps){
        super(props);
        this.state = {
            sidebar:true
        }
        this.handleToggle = this.handleToggle.bind(this);
    }

    handleToggle(){
        this.setState({
            sidebar:!this.state.sidebar
        })
    }

    render(): React.ReactNode {
       
        return (
            <div>
                <div className={this.state.sidebar? 'main-page z-index-10': 'main-page z-index-10 ml-0'}>
                    <AppBar position="fixed" className="header open">
                        <Toolbar className="justify-content-between">
                            <IconButton id="sidebar" edge="start" color="inherit" aria-label="menu" onClick={this.handleToggle}>
                                {this.state.sidebar? <MenuOpenIcon />:  <MenuIcon />}                                
                            </IconButton>                        
                            <div>
                                <IconButton color="inherit">
                                    <SettingsIcon />
                                </IconButton>
                                Hi Owen
                                <Button color="inherit"><ExitToAppIcon /></Button>
                            </div>
                        </Toolbar>
                    </AppBar>
                </div>
                {
                    this.state.sidebar? <div className="side-nav">
                    <div className="brand-logo">
                        <Link to="/">Shot Tracer</Link>
                    </div>
                    <Nav className="flex-column main-nav">
                        <Nav.Item>
                            <NavLink to="/Home" activeClassName="active" className="nav-link"><span className="flaticon-dashboard"></span> Dashboard</NavLink>
                        </Nav.Item>
                        <Nav.Item>
                            <NavLink to="/stats" activeClassName="active" className="nav-link"><span className="flaticon-edit"></span> stats</NavLink>
                        </Nav.Item>
                    </Nav>
                </div>: ''
                }
            </div>
        );
    }
}