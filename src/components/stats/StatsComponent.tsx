import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Paper from '@material-ui/core/Paper';
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { StatsProps } from './StatsProps';
import { StatsState } from './StatsState';


export default class StatsComponent extends React.Component<StatsProps, StatsState>{
    constructor(props:StatsProps){
        super(props);
        this.state = {
            statsTable:[
                {kid:0, name:"driver", subName:"", clubSpeed:113, attackAngle:-1.3, ballSpeed:113, smashFactory:1.3, launchAng:6.3, spinRate:1163, maxHeight:63, landAngle:54, carry:632},
                {kid:1, name:"3-wood", subName:"", clubSpeed:113, attackAngle:-1.3, ballSpeed:113, smashFactory:1.3, launchAng:6.3, spinRate:1163, maxHeight:63, landAngle:54, carry:632},
                {kid:2, name:"5-wood", subName:"", clubSpeed:113, attackAngle:-1.3, ballSpeed:113, smashFactory:1.3, launchAng:6.3, spinRate:1163, maxHeight:63, landAngle:54, carry:632},
                {kid:3, name:"Hybrid", subName:"15-18", clubSpeed:113, attackAngle:-1.3, ballSpeed:113, smashFactory:1.3, launchAng:6.3, spinRate:3113, maxHeight:63, landAngle:54, carry:632},
                {kid:4, name:"3 Iron", subName:"", clubSpeed:145, attackAngle:-1.6, ballSpeed:145, smashFactory:1.6, launchAng:5.6, spinRate:5145, maxHeight:45, landAngle:23, carry:145}
            ]
        }
    }
    render():React.ReactNode{
        return(
            <div className="main-page mt-100 pb-5">
                <Container fluid>
                    <Row>
                        <Col xs={12}>
                            <h3 className="mb-4 text-capitalize">stats</h3>
                            <div>
                                <TableContainer component={Paper}>
                                    <Table>
                                        <TableHead>
                                        <TableRow>
                                            <TableCell align="center"></TableCell>
                                            <TableCell align="center">Club Speed <span>mph</span></TableCell>
                                            <TableCell align="center">Attack Angle <span>degree</span></TableCell>
                                            <TableCell align="center">Ball Speed <span>mph</span></TableCell>
                                            <TableCell align="center">Smash Factory <span>mph</span></TableCell>
                                            <TableCell align="center">Launch Ang. <span>deg</span></TableCell>
                                            <TableCell align="center">Spin Rate <span>rpm</span></TableCell>
                                            <TableCell align="center">Max Height <span>yards</span></TableCell>
                                            <TableCell align="center">Land Angle <span>deg</span></TableCell>
                                            <TableCell align="center">Carry <span>yards</span></TableCell>
                                        </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                this.state.statsTable.map((TableData) => {
                                                    return<TableRow key={TableData.kid}>
                                                        <TableCell component="th" scope="row" align="center">
                                                            <strong className="text-capitalize text-nowrap">{TableData.name}</strong>
                                                            <small className="d-block">{TableData.subName}</small>
                                                        </TableCell>
                                                        <TableCell align="center">{TableData.clubSpeed}</TableCell>
                                                        <TableCell align="center">{TableData.attackAngle}'</TableCell>
                                                        <TableCell align="center">{TableData.ballSpeed}</TableCell>
                                                        <TableCell align="center">{TableData.smashFactory}</TableCell>
                                                        <TableCell align="center">{TableData.launchAng}'</TableCell>
                                                        <TableCell align="center">{TableData.spinRate}</TableCell>
                                                        <TableCell align="center">{TableData.maxHeight}</TableCell>
                                                        <TableCell align="center">{TableData.landAngle}'</TableCell>
                                                        <TableCell align="center">{TableData.carry}</TableCell>                                                    
                                                    </TableRow>
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}