import React from "react";
import { BrowserRouter, Router, Switch, Route } from 'react-router-dom';
import HeaderComponent from "../header/HeaderComponent";
import HomeComponent from "../home/HomeComponent";
import StatsComponent from "../stats/StatsComponent";
import SignInComponent from "../signin/SignInComponent";
import { RouterProps } from "./RouterProps";
import { RouterState } from "./RouterState";






export default class RouterComponent extends React.Component<RouterProps, RouterState>{
    constructor(props:RouterProps){
        super(props);
        this.state = {
            authenticated:false,
            username:""
        }
    }
    render(): React.ReactNode {
        return (
            <BrowserRouter>
                <div>
                {/* {this.state.authenticated ? <HeaderComponent username={this.state.username}/> : null} */}
                    <HeaderComponent />
                    <Switch>
                        <Route exact path="/" component={SignInComponent} />
                        <Route exact path="/Home" component={HomeComponent} />
                        <Route exact path="/stats" component={StatsComponent} />
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}
