import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from '@material-ui/core/Button';
import GooglePlayStore from '../../images/google-play.png'
import TextField from '@material-ui/core/TextField';
import { Link } from 'react-router-dom';
import GoogleIcon from '../../images/google.png';


export default class SignInComponent extends React.Component{
    render():React.ReactNode{
        return(
            <div className="login-page">
                <div className="bg-login"></div>
                <Container>
                    <Row>
                        <Col xs={6}>
                            <div className="login-left">
                                <h3>Welcome to </h3>
                                <h2>Shot Tracer</h2>
                                <p>Lorem Ipsum is simply dummy text of the printing</p>
                                <Button variant="contained" color="secondary" className="d-flex align-items-center text-left btn-google-pay mt-4">
                                    <img src={GooglePlayStore} alt="google play store" className="d-block pr-2"></img>                                                                        
                                    <span className="text"><small className="d-block text-uppercase">Get It on</small>Google Play</span>
                                </Button>
                                
                            </div>
                        </Col>
                        <Col xs="6">
                            <div className="login-right">
                                <h2 className="mb-2 mb-md-5">Sign in</h2>
                                <TextField label="Email Address" name="userName" required fullWidth className="mb-2 mb-sm-5 customForm" />
                                <div className="d-flex justify-content-end">
                                    <Button variant="contained" color="primary" className="rounded-pill">Continue</Button>
                                </div>
                                <div className="sigin-or">or</div>
                                <ul className="login-third-party">
                                    <li><Button variant="outlined">continue with google</Button></li>
                                    <li><Button variant="text">continue with facebook</Button></li>
                                    <li><Button variant="text">continue with apple</Button></li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}