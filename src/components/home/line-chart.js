import React, {Component} from 'react';
import { Chart } from "react-google-charts";



const date=[
    ['Month', 'User'],
    ["Jan", 0],
    ["Feb", 20],
    ["Mar", 43],
    ["Apr", 17],
    ["May", 68],
    ["Jun", 80],
    ["Jul", 41],
    ["Aug", 87],
    
];
const NewlineChartOptions={
    legend: {
        position: "none"       
    },   
    chartArea:{        
        left:"30",
        width:"100%"
    },
    
    curveType: 'none',
    pointShape:"circle",
    pointsVisible:true,
    series: [
        {color: '#fbc02d', strokeWidth:"1"}
    ],
    vAxis:{
        gridlines:{
            units:{
                months:["yy"]
            }
        }
    }
}
class LineChart extends Component{    
 
  render(){
    return(
        <div className="line-chart">
            <Chart
                width={'100%'} height={'210px'} chartType="LineChart" loader={<div>Loading Chart</div>}
                data={date} options={NewlineChartOptions}               
            />
        </div>
    )
  }
}


export default LineChart;
