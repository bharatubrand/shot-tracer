import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Card, CardContent, Typography, Box, LinearProgress, Grid } from '@material-ui/core';
import LineChart from './line-chart';
import ColumnChart from './ColumnChart';




export default class HomeComponent extends React.Component{
    render(): React.ReactNode{
        return(
            <div className="main-page mt-100 pb-5">
                <Container fluid>
                    <Row>
                        <Col xs={12}>
                            <h3 className="mb-4">Dashboard</h3>
                            <Row>
                                <Col lg={8} md={7}>
                                    <div>
                                        <Card>
                                            <CardContent>
                                                <Typography component="h6" variant="h6">
                                                    Lorem
                                                </Typography>
                                                <LineChart />
                                            </CardContent>                                            
                                        </Card>
                                    </div>
                                </Col>
                                <Col lg={4} md={5}>
                                    <div>
                                        <Card>
                                            <CardContent>
                                                <h6> Registrations</h6>
                                                <div className="registration">
                                                    <span className="d-block">Today</span>
                                                    <Box display="flex" alignItems="center">                                                        
                                                        <Box width="100%" className="progressbar">
                                                            <LinearProgress variant="determinate" value={80} />
                                                        </Box>
                                                        <Box minWidth={35}>
                                                            <Typography variant="body2" color="textSecondary" className="text-right">
                                                                80
                                                            </Typography>
                                                        </Box>
                                                    </Box>
                                                </div>
                                                <div className="registration">
                                                    <span className="d-block">Last Week</span>
                                                    <Box display="flex" alignItems="center">                                                        
                                                        <Box width="100%" className="progressbar bg-blue">
                                                            <LinearProgress variant="determinate" value={70} />
                                                        </Box>
                                                        <Box minWidth={35}>
                                                            <Typography variant="body2" color="textSecondary" className="text-right">
                                                                70
                                                            </Typography>
                                                        </Box>
                                                    </Box>
                                                </div>
                                                <div className="registration">
                                                    <span className="d-block">Last Month</span>
                                                    <Box display="flex" alignItems="center">                                                        
                                                        <Box width="100%" className="progressbar bg-warning">
                                                            <LinearProgress variant="determinate" value={75} />
                                                        </Box>
                                                        <Box minWidth={35}>
                                                            <Typography variant="body2" color="textSecondary" className="text-right">
                                                                75
                                                            </Typography>
                                                        </Box>
                                                    </Box>
                                                </div>
                                                <div className="registration">
                                                    <span className="d-block">Last 3 Month</span>
                                                    <Box display="flex" alignItems="center">                                                        
                                                        <Box width="100%" className="progressbar bg-primary-light">
                                                            <LinearProgress variant="determinate" value={90} />
                                                        </Box>
                                                        <Box minWidth={35}>
                                                            <Typography variant="body2" color="textSecondary" className="text-right">
                                                                90
                                                            </Typography>
                                                        </Box>
                                                    </Box>
                                                </div>
                                            </CardContent>                                            
                                        </Card>
                                    </div>
                                </Col>
                            </Row>
                        </Col>
                        <Col lg={8} md={7}>
                            <div className="mt-5">
                                <Grid spacing={3} container>
                                    <Grid item xs={4} container alignItems="stretch" direction="row">
                                        <Card className="w-100">
                                            <CardContent>
                                                <h4>Lorem</h4>
                                                <h2>$ 27323</h2>
                                                <p>*lorem ipsum dolar dfasdf dfgdfg dfg dfgfd g </p>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                    <Grid item xs={4} container alignItems="stretch" direction="row">
                                        <Card className="w-100">
                                            <CardContent>
                                                <h4>Lorem</h4>
                                                <h2>$ 27323</h2>
                                                <p>*lorem ipsum dolar</p>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                    <Grid item xs={4} container alignItems="stretch" direction="row">
                                        <Card className="w-100">
                                            <CardContent>
                                                <h4>Lorem</h4>
                                                <h2>$ 27323</h2>
                                                <p>*lorem ipsum dolar</p>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                </Grid>


                                <Grid spacing={3} container className="mt-4">
                                    <Grid item xs={4} container alignItems="stretch" direction="row">
                                        <Card className="w-100">
                                            <CardContent>
                                                <h4>Lorem</h4>
                                                <h2>$ 27323</h2>
                                                <p>*lorem ipsum dolar dfasdf dfgdfg dfg dfgfd g </p>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                    <Grid item xs={8} container alignItems="stretch" direction="row">
                                        <Card className="w-100">
                                            <ColumnChart />
                                        </Card>
                                    </Grid>
                                    
                                </Grid>
                            </div>
                        </Col>
                        <Col lg={4} md={5}>
                            <Card className="mt-5">
                                <CardContent>
                                    <h5>Recent Activities</h5>
                                    <ul className="recent-activity-list">
                                        <li>
                                            <div className="title">
                                                Golf
                                                <span>05:30 AM</span>
                                            </div>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                        </li>
                                        <li>
                                            <div className="title">
                                                Golf
                                                <span>05:30 AM</span>
                                            </div>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                        </li>
                                        <li>
                                            <div className="title">
                                                Golf
                                                <span>05:30 AM</span>
                                            </div>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                                        </li>
                                        
                                    </ul>
                                </CardContent>                                

                            </Card>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}


